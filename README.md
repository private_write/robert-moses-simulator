# Robert Moses Simulator


![Demo Video](demo.mp4)

A clone of three puzzles from the puzzle game "Rush Hour". Made in Unity with C# in 48 hours for my Game Dev class (time was not part of the assignment). Probably has some bugs. Probably not great code. Probably terribly abusive of the Unity TileMap. All game logic in one C# script. 

## Assignment writeup: 

Puzzle choice for the assignment was inspired by remembering the game Rush Hour while people were talking about cars and urban planning in the Comp Sci lab. I did not use many resources aside from referencing some puzzle layouts from the original board game and taking a good, hard look at the Unity Tilemap Scripting API page. If I could do it over again, I would add a game manager layer of abstraction holding the board state independently of the TileMap, since the TileMap is difficult to interface with, and batch changes into writes and refreshes to the Unity Tilemap on updating the game state.
