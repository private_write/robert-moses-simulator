using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;

public class CarManager : MonoBehaviour
{
		public Tile selectedTile; // the tile for selected road tiles
		public Tile pavement;
		public TextMeshProUGUI pickedup; // textbox to display what is held
		
		Tilemap carTm; // all car tiles including exit tiles
		
		bool movingCar; // are we in car placement mode?
		Tile clickedTile; // the selected tile type
		string cartype; // picked up vehicle name
		bool horizontalCar; // is vehicle horizontal?
		int carLength; // how long is the held vehicle?
		Vector3Int tilemapPos; // position of clickedTile on grid
		bool moveInit; // Initialize the moving phase
    // Start is called before the first frame update
    void Start()
    {
				// assuming this script is attached to the cars Tilemap
        carTm = GetComponent<Tilemap>();
				movingCar = false;
				moveInit = false;
				horizontalCar = false;
				cartype = "";
				tilemapPos = new Vector3Int(-1, -1, -1);
    }

    // Update is called once per frame
    void Update()
    {
				if (movingCar) {
						// initialize car movement process
						if (!moveInit) {
								// check win condition after each move
								string adjExitName = carTm.GetTile(new Vector3Int(0,3,0)).name;
								if ((adjExitName == "pavement-selected") && horizontalCar && (cartype == "redcar")) {
										// Puzzle Solved!
										Debug.Log("Puzzle Solved!");
								}
								moveInit = true;
						}
				} else {
						pickedup.text = "None";
				}
    }

		// Instead of doing this, instantiate a sprite of the full vehicle lifted from tiles
		// in the tilemap here into a sprite gameObject following the mouse and delete
		// tiles which match cartype to start moving a car.

		// create new tiles in the tilemap later when depositing the car.
		void OnMouseDown()
		{
				if (!movingCar) { // we are now going to move car mode
						// Identify which tile got clicked
						tilemapPos = carTm.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
						clickedTile = carTm.GetTile<Tile>(tilemapPos);
						cartype = clickedTile.name;
						if (!cartype.Contains("pavement")) {
								// check if the car is facing horizontal rather than vertical
								if (!cartype.Contains("vert")) { horizontalCar = true;} else {horizontalCar = false;}
								// do not exceed grid bounds
								int minbound = -3;
								int maxbound = 2;
								// work out the selected moveable tiles. lots of duplicate code in loops here
								Vector3Int cTilePos = tilemapPos;
								carLength = 0;
								if (horizontalCar) {
										for (int i = tilemapPos.x; i >= minbound; i--) {
												// middle-out from center to left
												cTilePos = new Vector3Int(i, cTilePos.y, cTilePos.z);
												string cTileName = carTm.GetTile(cTilePos).name;
												if (!(cTileName == cartype)) {
														// check if there is another car here, do not delete it
														if ((!cTileName.Contains("pavement"))) {
																break;
														}
														carTm.SetTile(cTilePos, selectedTile);
												} else {
														carLength += 1;
														carTm.SetTile(cTilePos, selectedTile);
												}
										}
										cTilePos = tilemapPos;
										for (int i = tilemapPos.x; i <= maxbound; i++) {
												// middle-out from center to right
												cTilePos = new Vector3Int(i, cTilePos.y, cTilePos.z);
												TileBase cTileName = carTm.GetTile(cTilePos);
												if (cTileName != null) {
														if (cTileName.name != cartype) {
																// check if there is another car here, do not delete it
																if ((!cTileName.name.Contains("pavement"))) {
																		break;
																}
																carTm.SetTile(cTilePos, selectedTile);
														}
												} else {
														carLength += 1;
														carTm.SetTile(cTilePos, selectedTile);
												}
										}
								} else {
										for (int i = tilemapPos.y; i >= minbound; i--) {
												// middle-out from center to top
												cTilePos = new Vector3Int(cTilePos.x, i, cTilePos.z);
												string cTileName = carTm.GetTile(cTilePos).name;
												if (!(cTileName == cartype)) {
														// check if there is another car here, do not delete it
														if ((!cTileName.Contains("pavement"))) {
																break;
														}
														carTm.SetTile(cTilePos, selectedTile);
												} else {
														carLength += 1;
														carTm.SetTile(cTilePos, selectedTile);
												}
										}
										cTilePos = tilemapPos;
										for (int i = tilemapPos.y; i <= maxbound; i++) {
												// middle-out from center to bottom
												cTilePos = new Vector3Int(cTilePos.x, i, cTilePos.z);
												string cTileName = carTm.GetTile(cTilePos).name;
												if (!(cTileName == cartype)) {
														// check if there is another car here, do not delete it
														if ((!cTileName.Contains("pavement"))) {
																break;
														}
														carTm.SetTile(cTilePos, selectedTile);
												} else {
														carLength += 1;
														carTm.SetTile(cTilePos, selectedTile);
												}
										}
								}
								// Apply changes
								carTm.RefreshAllTiles();
								if (carLength == 2) {
										pickedup.text = cartype;
								} else {
										string busText = cartype.Replace("car","bus");
										pickedup.text = busText;
								}
								movingCar = true;
						}
				} else { // we are already in move-mode, place car
						Vector3Int placePos = carTm.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
						Tile placeTile = carTm.GetTile<Tile>(placePos);
						int minbound = -2;
						int maxbound = 3;
						Vector3Int farthestPoint;
						if (horizontalCar) {
								farthestPoint = new Vector3Int(placePos.x+carLength-1, placePos.y, placePos.z);
						} else {
								farthestPoint = new Vector3Int(placePos.x, placePos.y+carLength-1, placePos.z);
						}
						bool carHere = (placeTile.name.Contains("car")) || (carTm.GetTile<Tile>(farthestPoint).name.Contains("car"));
						// so there is a loophole here, s.t. busses can be placed over cars if the car
						// is at the midsection
						bool lowerBoundFailed = (placePos.x < minbound-1) || (placePos.y < minbound-1);
						bool upperBoundFailed;
						if (carLength != 3) {upperBoundFailed = (farthestPoint.x > maxbound-1) || (farthestPoint.y > maxbound-1);}
						else {upperBoundFailed = (farthestPoint.x > maxbound) || (farthestPoint.y > maxbound-1);}
						if (lowerBoundFailed || upperBoundFailed || carHere) {
								// Not allowed!
								Debug.Log("Can't place there");
								Debug.Log("Lower: "+lowerBoundFailed+" Upper: "+upperBoundFailed+" car?: "+carHere);
								Debug.Log("FarthestPoint: "+farthestPoint);
						} else {
								Vector3Int cPlacePos = new Vector3Int(placePos.x, placePos.y, placePos.z);
								if (horizontalCar) {
										for (int i = placePos.x; i <= maxbound; i++) {
												cPlacePos.x = i;
												if (carLength != 0) {
														carTm.SetTile(cPlacePos, clickedTile);
														carLength -= 1; // place that length of car
												} else {break;}
										}
								} else {
										for (int i = placePos.y; i <= maxbound; i++) {
												cPlacePos.y = i;
												if (carLength != 0) {
														carTm.SetTile(cPlacePos, clickedTile);
														carLength -= 1; // place that length of car
												} else {break;}
										}
								}
								// Clean up:
								// TODO: reset all placement tiles to normal pavement
								List<Vector3> tileWorldLocations = new List<Vector3>();

								foreach (var pos in carTm.cellBounds.allPositionsWithin) {
										Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
										Vector3 place = carTm.CellToWorld(localPlace);
										if (carTm.HasTile(localPlace))
										{
												tileWorldLocations.Add(place);
										}
								}
								foreach (Vector3 tileLoc in tileWorldLocations) {
										Vector3Int coord = carTm.WorldToCell(tileLoc);
										string tileType = carTm.GetTile(coord).name;
										if (tileType == "pavement-selected") {
												carTm.SetTile(coord, pavement);
										}
								}
								carTm.RefreshAllTiles();
								movingCar = false;
								moveInit = false;
						}
				}
		}
}
